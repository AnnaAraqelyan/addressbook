//
//  AppDelegate.h
//  AddressBook
//
//  Created by Anna on 10.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString * BASE_URL;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

