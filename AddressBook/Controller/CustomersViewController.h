//
//  CustomersViewController.h
//  AddressBook
//
//  Created by Anna on 12.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomersViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblForCollapse;
@property (nonatomic, retain) NSArray *arrayOriginal;
@property (nonatomic, retain) NSMutableArray *arForTable;
-(void)miniMizeThisRows:(NSArray*)ar;
@property (strong, nonatomic)UIImage * empImage;
@property (weak, nonatomic)UIView * cellView;
@property (strong, nonatomic)UIImageView * cellImageView;
@property (weak, nonatomic)NSString * titletext;
@property (weak, nonatomic)NSString * phonetext;
@property (weak, nonatomic)NSString * mailtext;



@end

NS_ASSUME_NONNULL_END
