//
//  InfoViewController.h
//  AddressBook
//
//  Created by Anna on 20.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
NS_ASSUME_NONNULL_BEGIN

@interface InfoViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *infoImageView;

@property (weak, nonatomic) IBOutlet UITextField *infoTitleTfd;
@property (weak, nonatomic) IBOutlet UIButton *infoPhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *infoMailBtn;
@property (weak, nonatomic) UIImage * infoimage;
@property (weak, nonatomic) NSString * infophone;
@property (weak, nonatomic) NSString * infomail;
@property (weak, nonatomic) NSString * titlestr;

@end

NS_ASSUME_NONNULL_END
