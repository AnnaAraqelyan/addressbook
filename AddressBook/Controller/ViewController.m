//
//  ViewController.m
//  AddressBook
//
//  Created by Anna on 10.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import "ViewController.h"
#import <Realm/Realm.h>
#import <Realm.h>
#import "AppDelegate.h"
#import <Security/SecKey.h>
#import "KeychainItemWrapper.h"
#import <CFNetwork/CFNetwork.h>
@interface ViewController () <UITextFieldDelegate> {
    __weak IBOutlet UITextField *userNameTxtField;
    __weak IBOutlet UITextField *passwordTxtField;
    __weak IBOutlet UIButton *signInBttn;
    BOOL isSaved;
    
    UITextField * textF;
    __weak IBOutlet UIButton *saveButton;
}



@end

@implementation ViewController
-(void)viewWillAppear:(BOOL)animated {
    self.userNameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString* userPlaceholder = @"username";
    userNameTxtField.backgroundColor = [UIColor clearColor];
    userNameTxtField.borderStyle = UITextBorderStyleRoundedRect;
    userNameTxtField.layer.borderColor = [UIColor whiteColor].CGColor;
    userNameTxtField.layer.borderWidth = 1;
    userNameTxtField.layer.cornerRadius = 10;
    [userNameTxtField setPlaceholder:userPlaceholder];
    [userNameTxtField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    userNameTxtField.placeholder = userPlaceholder;
    userNameTxtField.leftViewMode = UITextFieldViewModeAlways;
    UIImageView * imgv1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"username"]];
    imgv1.frame = CGRectMake(8, 0, 15, 15);
    userNameTxtField.leftView = [[UIView alloc]initWithFrame:imgv1.frame];
    userNameTxtField.leftView = [self paddingViewWithImage:imgv1 andPadding:8.0f];
    userNameTxtField.leftView.contentMode = UIViewContentModeScaleAspectFill;
    userNameTxtField.leftView.userInteractionEnabled = YES;
    
    
    NSString* passwordPlaceholder = @"password";
    passwordTxtField.backgroundColor = [UIColor clearColor];
    passwordTxtField.borderStyle = UITextBorderStyleRoundedRect;
    passwordTxtField.layer.borderColor = [UIColor whiteColor].CGColor;
    passwordTxtField.layer.borderWidth = 1;
    passwordTxtField.layer.cornerRadius = 10;
    [passwordTxtField setPlaceholder:passwordPlaceholder];
    [passwordTxtField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    passwordTxtField.placeholder = passwordPlaceholder;
    passwordTxtField.leftViewMode = UITextFieldViewModeAlways;
    UIImageView * imgv2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    imgv2.frame = CGRectMake(8, 0, 15, 15);
    passwordTxtField.leftView = [[UIView alloc]initWithFrame:imgv2.frame];
    passwordTxtField.leftView = [self paddingViewWithImage:imgv2 andPadding:8.0f];
    passwordTxtField.leftView.contentMode = UIViewContentModeScaleAspectFill;
    passwordTxtField.leftView.userInteractionEnabled = YES;
    
    
    signInBttn.layer.borderWidth = 1;
    signInBttn.layer.borderColor = [UIColor whiteColor].CGColor;
    signInBttn.layer.cornerRadius = 10;
    [self textFieldShouldReturn:passwordTxtField];
    isSaved = false;
    // Do any additional setup after loading the view, typically from a nib.
}
-(UIView*)paddingViewWithImage:(UIImageView*)imageView andPadding:(float)padding
{
    float height = CGRectGetHeight(imageView.frame);
    float width =  CGRectGetWidth(imageView.frame) + padding;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    [paddingView addSubview:imageView];
    
    return paddingView;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger next = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:next];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [saveButton setSelected:NO];
    saveButton.backgroundColor = UIColor.clearColor;
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    textF = textField;
    if ([textField.accessibilityIdentifier isEqual:@"id"]){
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    }
}
-(void)keyboardDidShow:(NSNotification *)notification{
    CGFloat haight = self.view.frame.size.height;
    if (haight <= 568 && [textF.accessibilityIdentifier isEqualToString:@"id"]){
        [self.view setFrame:CGRectMake(0, -110, self.view.frame.size.width, self.view.frame.size.height)];
    }
}
-(void)keyboardDidHide:(NSNotification*)notification{
    CGFloat haight = self.view.frame.size.height;
    if (haight <= 568 && [textF.accessibilityIdentifier isEqualToString:@"id"]){
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
   
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title
                                                       message:message
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
    });

    
}

- (void) alertStatus:(NSString *)msg :(NSString *)title :(int) tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
}
-(void)loginRequest: (NSString*)login andPassword:(NSString*)password{
    self.userNameTextField.text = login;
    self.passwordTextField.text = password;
    NSString * urlstring = [NSString stringWithFormat:@"https://contact.taxsee.com/Contacts.svc/Hello?login=%@&password=%@", login,password];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:urlstring];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //NSDictionary* headerFields = [httpResponse allHeaderFields];
        if (httpResponse.statusCode == 200) {
            if (self->isSaved == true){
                
                [self performSegueWithIdentifier:@"toNext" sender:nil];
            }else{
                [self showAlertWithMessage:@"Please save login and password and try again" andTitle:@"There is no saved login"];
            }

        } else {
            
            [self showAlertWithMessage:@"Incorrect login or password" andTitle:@""];
        }
        
    }];
    [postDataTask resume];
}

- (IBAction)loginButtonAction:(id)sender {
    [userNameTxtField resignFirstResponder];
    [passwordTxtField resignFirstResponder];
    if ([self.userNameTextField.text length] == 0 || [self.passwordTextField.text length] == 0){
        [self showAlertWithMessage:@"All fields are required" andTitle:@""];
    
   
       
    }else if([self.userNameTextField.text length] != 0 && [self.passwordTextField.text length] != 0)
    {
        [self loginRequest:userNameTxtField.text andPassword:passwordTxtField.text];
        
        
   }
}

- (IBAction)savePassword:(id)sender {
    isSaved = true;
    if (isSaved){
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"KeychainItem" accessGroup:nil];
    [keychainItem setObject:passwordTxtField.text forKey:(__bridge id)kSecValueData];
    [keychainItem setObject:userNameTxtField.text forKey:(__bridge id)kSecAttrAccount];
    NSString *pass = [keychainItem objectForKey:(__bridge id)kSecValueData];
    NSString *username = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
        
        [saveButton setSelected:YES];
        //saveButton.titleLabel.text = @"Saved";
        [saveButton setTitle:@"Saved" forState:UIControlStateSelected];
        
        isSaved = true;

        
    }

}
@end
