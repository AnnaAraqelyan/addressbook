//
//  CustomersViewController.m
//  AddressBook
//
//  Created by Anna on 12.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import "CustomersViewController.h"
#import "ViewController.h"
#import "InfoViewController.h"
#import "KeychainItemWrapper.h"
#import <Security/Security.h>
@interface CustomersViewController (){
NSDictionary * dictionary;
    __weak IBOutlet UITableView *tableView1;
 
}
@end

@implementation CustomersViewController
- (void)viewDidLoad {
    [super viewDidLoad];

    [self requestWithURL:[NSString stringWithFormat:@"https://contact.taxsee.com/Contacts.svc/GetAll?login=test_user&password=test_pass"]];

}

-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title
                                                       message:message
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
    });
    
    
}
NSMutableDictionary * imagesDic;
NSMutableArray * arr;
-(void)requestWithURL:(NSString*)urlstring {
 
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:urlstring];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //NSDictionary* headerFields = [httpResponse allHeaderFields];
        if (httpResponse.statusCode == 200) {
            //NSLog(@" %@ RESPONSE " , response);
            
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            // print as json
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            //NSLog(@"JJJJJJJJJSSSSSSOOOOOONNN %@ ", jsonString);
            NSInteger index = 0;
            NSMutableArray * dTmp = [[NSMutableArray alloc]init];
            NSMutableArray * idArray = [[NSMutableArray alloc]init];
          
            imagesDic = [[NSMutableDictionary alloc]init];

            for (NSDictionary * value in [json valueForKey:@"Departments"]  ) {
                NSMutableArray * idName = [[NSMutableArray alloc] init];
                NSLog(@" DEPARTMENTS ARE %@ ", value);
               NSString* depName = [value valueForKey:@"Name"];
                
                if([json valueForKey:@"Departments"]  != nil && [value valueForKey:@"Departments"] != nil){
                    [dTmp addObject:value];
//                    NSDictionary * depOfdeps = [value valueForKey:@"Departments"];
//                   [dTmp addObject:depOfdeps];
                    

                }else{
                    [dTmp addObject:value];

                }
                if ([value valueForKey:@"Employees"] != nil ){
                    idArray = [value valueForKey:@"Employees"];
           
                }else if ([value valueForKey:@"Departments"] != nil){
                    NSDictionary* depArray = [value valueForKey:@"Departments"];
                    idArray = [depArray valueForKey:@"Employees"];

                }
                for (NSUInteger i = 0; i < idArray.count; i++){
                    [idName addObject: [[idArray objectAtIndex: i] valueForKey:@"ID"] ];
                }
                if (idName.count > 0) {
                    [imagesDic setObject:idName forKey:depName];
                }
                self.arForTable = [[NSMutableArray alloc] init];
                [self.arForTable addObjectsFromArray:dTmp];
                
                index++;
 
            }
          
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->tableView1 reloadData];
            });
            NSLog(@" DDDDDTTTTTEEEEEEMMMMPPPP %@" , dTmp);
        } else {
            
            [self showAlertWithMessage:@"OOOpppssss" andTitle:@""];
        }
        
    }];
   
    [postDataTask resume];

}


-(void)uploadImage{
    NSString* idString;
    UIImage * imageToSelf;
    NSMutableDictionary * allImgDic = [NSMutableDictionary dictionary];
    for (NSInteger i = 0 ; i < imagesDic.allKeys.count; i++)  {
        NSMutableArray * imageArr = [[NSMutableArray alloc] init];
        NSArray * imagArr = [imagesDic valueForKey:imagesDic.allKeys[i]];
        //dispatch_semaphore_t semafor = dispatch_semaphore_create(0);

        for (NSString * imgId in imagArr) {
            idString = imgId;
            NSString *strImgURLAsString = [NSString stringWithFormat:@"https://contact.taxsee.com/Contacts.svc/GetWPhoto?login=test_user&password=test_pass&id=%@",idString];
            [strImgURLAsString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imgURL = [NSURL URLWithString:strImgURLAsString];
//            NSData * imageData = [NSData dataWithContentsOfURL:imgURL];
//            UIImage *imgageWithData = [[UIImage alloc] initWithData:imageData];
//            self->empImage = imgageWithData;
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:imgURL
                    completionHandler:^(NSData *data,
                                        NSURLResponse *response,
                                        NSError *error) {
                        
                        if (!error) {
                            UIImage *img = [[UIImage alloc] initWithData:data];
                            self->_empImage = img;
                            [imageArr addObject:img];
                            //dispatch_semaphore_signal(semafor);
                        }else{
                            NSLog(@"%@",error);
                        }
                }] resume];
           // dispatch_semaphore_wait(semafor, DISPATCH_TIME_FOREVER);
        }
        [allImgDic setObject:imageArr forKey:imagesDic.allKeys[i]];
    }
    imageToSelf = self.empImage;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.arForTable count];
}
NSMutableArray * ar;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.indentationLevel = 0;
        cell.indentationWidth = 0;
    }
    
    if ([[self.arForTable objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {

        if ( [self.arForTable objectAtIndex:indexPath.row][@"Departments"] != nil){
            NSLog(@"%ld INDEXPATH ROW IS ", (long)indexPath.row);

            NSArray * otdelArray = [[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Departments"];
            
            //cell.textLabel.text = [[otdelArray objectAtIndex:indexPath.row] valueForKey:@"Name"];
            cell.textLabel.text=[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Name"];

            NSLog(@"%@ CEEElll TEXT IS ", cell.textLabel.text);
            [cell setIndentationLevel:[[[[self.arForTable objectAtIndex:indexPath.row]valueForKey:@"Departments"] valueForKey:@"Employees"] count]];
            
        } else{
            cell.textLabel.text=[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Name"];
            [cell setIndentationLevel:[[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Employees"] count]];
        }
       
    }else {
        if ( [self.arForTable objectAtIndex:indexPath.row] != nil ){
            NSArray * arrCell = [[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Name"] ;

            for (NSInteger i = 0; i < arrCell.count; i++){
                cell.textLabel.text = [arrCell objectAtIndex:i];
           
                [cell setIndentationLevel:[[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Employees"] count]];
                
        }
         
        } else{
            cell.textLabel.text=[[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Name"] objectAtIndex:indexPath.row];
            [cell setIndentationLevel:[[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Employees"] count]];
        }


    }
    
    return cell;
}
NSIndexPath * indPath;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    indPath = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self uploadImage];
    ar = [[NSMutableArray alloc] init];
 if ([[self.arForTable objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
    NSDictionary *d=[self.arForTable objectAtIndex:indexPath.row];
  if ( [self.arForTable objectAtIndex:indexPath.row][@"Departments"] != nil){
     
      for (NSDictionary * empDic in [d valueForKeyPath:@"Departments"]  ) {
          if (empDic != nil){
          [ar addObject: empDic];
          }

      }
     
  } else if([d valueForKeyPath:@"Employees"]) {
        
        for (NSDictionary * empDic in [d valueForKeyPath:@"Employees"] ) {
            [ar addObject: empDic];
            
        }
    }
   
 }else{
     if ( [self.arForTable objectAtIndex:indexPath.row] != nil ){
         NSArray * arrayofArray = [[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"Employees"];
          for (NSInteger i = 0; i < arrayofArray.count; i++){
              [ar addObject: [arrayofArray objectAtIndex:i]];
              NSLog(@" AAAAAAAAARRRRRAAAAYYYYYY %lu" , (unsigned long)ar.count);
 

          }
        
     }
     
 }
        BOOL isAlreadyInserted=NO;
        
        for(NSDictionary *dInner in ar ){
            if (![dInner isKindOfClass:[NSNull class]] ){

            NSInteger index=[self.arForTable indexOfObjectIdenticalTo:dInner];
            
                       isAlreadyInserted=(index>0 && index!=NSIntegerMax);
            if(isAlreadyInserted) break;
            }
        }
        
        if(isAlreadyInserted) {
  
        [self miniMizeThisRows:ar];
            
        } else {

            UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
            if (([cell indentationLevel] == 0) && ([self.arForTable objectAtIndex:indexPath.row][@"Title"] != nil  || [self.arForTable objectAtIndex:indexPath.row][@"Phone"] != nil) ) {
                if ( [self.arForTable objectAtIndex:indexPath.row][@"Phone"] != nil){
                    NSLog(@" PHOOONNNEE %@" , [self.arForTable objectAtIndex:indexPath.row][@"Phone"]);
                    _phonetext = [self.arForTable objectAtIndex:indexPath.row][@"Phone"];
                }
                if ( [self.arForTable objectAtIndex:indexPath.row][@"Email"] != nil){
                    NSLog(@" EEMMMAAIIL %@" , [self.arForTable objectAtIndex:indexPath.row][@"Email"]);
                    _mailtext = [self.arForTable objectAtIndex:indexPath.row][@"Email"];
                }
                if ( [self.arForTable objectAtIndex:indexPath.row][@"Title"] != nil){
                    NSLog(@" TITLEEEEEE %@" , [self.arForTable objectAtIndex:indexPath.row][@"Title"]);
                    _titletext = [self.arForTable objectAtIndex:indexPath.row][@"Title"];
                }
                
                _cellImageView.image = _empImage;
                
                            [self performSegueWithIdentifier:@"fromCellToView" sender:nil];
            }
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arCells=[NSMutableArray array];

            for(NSDictionary *dInner in ar ) {
                
                [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                if (![dInner isKindOfClass:[NSNull class]] ){
                [self.arForTable insertObject:dInner atIndex:count++];
                }
              }
          
            [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationLeft];
        }
  

    }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"fromCellToView"]){
        InfoViewController * infovc = segue.destinationViewController;
        
        [infovc setInfoimage:_empImage];
        [infovc setInfomail:_mailtext];
        [infovc setInfophone:_phonetext];
        [infovc setTitlestr:_titletext];
    }
}

-(void)miniMizeThisRows:(NSArray*)ar{
    NSUInteger indexToRemove = 0;
    for(NSDictionary *dInner in ar ) {
        if (![dInner isKindOfClass:[NSNull class]] ){
            indexToRemove=[self.arForTable indexOfObjectIdenticalTo:dInner];


        NSArray *arInner=[dInner valueForKey:@"Employees"];
        NSArray *arrOtdels = [dInner valueForKey:@"Departments"] ;
        if(arInner && [arInner count]>0){
            [self miniMizeThisRows:arInner];

        }
            if(arrOtdels && [arrOtdels count]>0){
                [self miniMizeThisRows:arrOtdels];
                
                
            }
        
        if([self.arForTable indexOfObjectIdenticalTo:dInner]!=NSNotFound) {
            [self.arForTable removeObjectIdenticalTo:dInner ];
            [tableView1 deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                         [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                         ]
                                       withRowAnimation:UITableViewRowAnimationRight];
        }
    }
}
}

- (IBAction)logOutbutton:(id)sender {
    
        KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"KeychainItem" accessGroup:nil];
    [keychainItem setObject:@"" forKey:(__bridge id)kSecValueData];
    [keychainItem setObject:@"" forKey:(__bridge id)kSecAttrAccount];
    [keychainItem resetKeychainItem];
    [self performSegueWithIdentifier:@"tologin" sender:nil];
   // [self dismissViewControllerAnimated:YES completion:nil];

}


@end
