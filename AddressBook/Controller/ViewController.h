//
//  ViewController.h
//  AddressBook
//
//  Created by Anna on 10.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property NSMutableDictionary *requestParameters;
@property NSString *relativeURL;

@end

