//
//  InfoViewController.m
//  AddressBook
//
//  Created by Anna on 20.11.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   

//    _infoMailBtn.titleLabel.text = _infomail;
//    _infoPhoneBtn.titleLabel.text = _infophone;
//    _infoTitleTfd.text = _titlestr;
    
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    self.infoImageView.image = _infoimage;
    self.infoTitleTfd.textColor = UIColor.blackColor;
    
    [self.infoTitleTfd setText:_titlestr];
    
    [self.infoPhoneBtn setTitle:_infophone forState:UIControlStateNormal];
    [self.infoMailBtn setTitle:_infomail forState:UIControlStateNormal];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)sendEmail {
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    if([MFMailComposeViewController canSendMail]) {
       
        [mailCont setSubject:@"Message!"];
        [mailCont setToRecipients:[NSArray arrayWithObject:self.infoMailBtn.titleLabel.text]];
        [mailCont setMessageBody:@"Message" isHTML:NO];
        
        [self presentModalViewController:mailCont animated:YES];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)backToTableView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)phoneCall:(id)sender {
    NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",self.infoPhoneBtn.titleLabel.text];
    NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL options:nil completionHandler:nil];
}
- (IBAction)sendMail:(id)sender {
    [self sendEmail];
}

@end
